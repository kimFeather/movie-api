'use strict'

const express = require('express')
const app = express()

const port = 8000

app.get('/movies',(req,res) => {
    const result = {
        result : [{
            titel: 'fake title1',
            image_url:'kuy',
            overview: 'fake title1'
        },{
            titel: 'fake title2',
            image_url:'fake title2',
            overview: 'fake title2'
        }]
    }
    res.json({result})
})

app.listen(port, () =>
 console.log (`started at ${port}`))
